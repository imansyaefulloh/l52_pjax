@extends('layout')

@section('content')
    
    <h1>Contact Page</h1>
    
    <hr>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div>
                <form action="">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" class="form-control" value="">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Contact</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-6">
            <div>Sidebar</div>
        </div>
    </div>

@stop